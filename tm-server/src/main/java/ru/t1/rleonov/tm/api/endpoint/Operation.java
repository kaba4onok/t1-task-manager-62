package ru.t1.rleonov.tm.api.endpoint;

import ru.t1.rleonov.tm.dto.request.AbstractRequest;
import ru.t1.rleonov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
