package ru.t1.rleonov.tm.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.User;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findFirstByLogin(@Nullable final String login);

    @Nullable
    User findFirstByEmail(@Nullable final String email);

}
