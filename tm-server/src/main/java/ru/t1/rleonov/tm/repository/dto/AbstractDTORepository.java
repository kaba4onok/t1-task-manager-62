package ru.t1.rleonov.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;

@Repository
@Scope("prototype")
public interface AbstractDTORepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {

}
