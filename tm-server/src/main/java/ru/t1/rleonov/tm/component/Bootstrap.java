package ru.t1.rleonov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.api.endpoint.*;
import ru.t1.rleonov.tm.api.service.*;
import ru.t1.rleonov.tm.endpoint.*;
import ru.t1.rleonov.tm.util.SystemUtil;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @SneakyThrows
    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initEndpoints();
        initPID();
        loggerService.info("***WELCOME TO TASK-MANAGER***");
        loggerService.startJmsLoggerProducer();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("***TASK-MANAGER IS SHUTTING DOWN***");
    }

}
