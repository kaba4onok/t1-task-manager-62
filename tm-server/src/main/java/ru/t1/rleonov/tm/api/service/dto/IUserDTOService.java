package ru.t1.rleonov.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import java.util.Collection;
import java.util.List;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    @SneakyThrows
    List<UserDTO> findAll();

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login);

    void set(@NotNull Collection<UserDTO> users);

    void clear();

}
