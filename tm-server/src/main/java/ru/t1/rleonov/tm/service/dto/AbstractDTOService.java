package ru.t1.rleonov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.rleonov.tm.api.service.dto.IDTOService;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;

@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO> implements IDTOService<M> {

}
