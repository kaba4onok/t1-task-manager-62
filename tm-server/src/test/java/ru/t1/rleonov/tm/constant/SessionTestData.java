package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.model.SessionDTO;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;


public final class SessionTestData {

    @NotNull
    public final static SessionDTO SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION_ADMIN = new SessionDTO();

    @NotNull
    public final static SessionDTO SESSION_TEST = new SessionDTO();

    @NotNull
    public final static List<SessionDTO> SESSIONS = Arrays.asList(SESSION1, SESSION2, SESSION_ADMIN);

    @NotNull
    public final static List<SessionDTO> USER1_SESSIONS = Collections.singletonList(SESSION1);

    @NotNull
    public final static List<SessionDTO> USER2_SESSIONS = Collections.singletonList(SESSION2);

    static {
        SESSION1.setUserId(USER1.getId());
        SESSION2.setUserId(USER2.getId());
        SESSION_ADMIN.setUserId(ADMIN.getId());
    }

}
