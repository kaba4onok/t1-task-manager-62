package ru.t1.rleonov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.rleonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.rleonov.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.api.service.dto.IUserDTOService;
import ru.t1.rleonov.tm.configuration.ServerConfiguration;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.rleonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.marker.UnitCategory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.ProjectTestData.ADMIN_PROJECTS;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.*;
import static ru.t1.rleonov.tm.constant.UserTestData.ADMIN;

@Category(UnitCategory.class)
public final class ProjectTaskDTOServiceTest {

    @Nullable
    private static IProjectDTOService PROJECT_SERVICE;

    @Nullable
    private static IUserDTOService USER_SERVICE;

    @Nullable
    private static ITaskDTOService TASK_SERVICE;

    @Nullable
    private static IProjectTaskDTOService PROJECT_TASK_SERVICE;

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @NotNull
    private static String ADMIN_ID = "";

    @NotNull
    private final static List<ProjectDTO> PROJECTS = new ArrayList<>();

    @NotNull
    private final static List<TaskDTO> TASKS = new ArrayList<>();

    @BeforeClass
    @SneakyThrows
    public static void setUp() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        PROJECT_SERVICE = context.getBean(IProjectDTOService.class);
        TASK_SERVICE = context.getBean(ITaskDTOService.class);
        PROJECT_TASK_SERVICE = context.getBean(IProjectTaskDTOService.class);
        USER1_ID = USER_SERVICE.create(USER1.getLogin(), USER1.getPassword(), USER1.getEmail()).getId();
        USER2_ID = USER_SERVICE.create(USER2.getLogin(), USER2.getPassword(), USER2.getEmail()).getId();
        ADMIN_ID = USER_SERVICE.create(ADMIN.getLogin(), ADMIN.getPassword(), ADMIN.getEmail()).getId();
        USER1_PROJECTS.forEach(project -> project.setUserId(USER1_ID));
        USER2_PROJECTS.forEach(project -> project.setUserId(USER2_ID));
        ADMIN_PROJECTS.forEach(project -> project.setUserId(ADMIN_ID));
        PROJECTS.addAll(USER1_PROJECTS);
        PROJECTS.addAll(USER2_PROJECTS);
        PROJECTS.addAll(ADMIN_PROJECTS);
        PROJECT_SERVICE.set(PROJECTS);
    }

    @AfterClass
    @SneakyThrows
    public static void reset() {
        PROJECT_SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Before
    @SneakyThrows
    public void before() {
        USER1_TASKS.forEach(task -> task.setUserId(USER1_ID));
        USER2_TASKS.forEach(task -> task.setUserId(USER2_ID));
        ADMIN_TASKS.forEach(task -> task.setUserId(ADMIN_ID));
        USER1_TASK3.setUserId(USER1_ID);
        TASKS.addAll(USER1_TASKS);
        TASKS.addAll(USER2_TASKS);
        TASKS.addAll(ADMIN_TASKS);
        TASK_SERVICE.set(TASKS);
    }

    @After
    @SneakyThrows
    public void after() {
        TASK_SERVICE.clear();
    }

    @Test
    public void bindTaskToProject() {
        @Nullable final String userId = USER2_TASK1.getUserId();
        @NotNull final String projectId = USER2_PROJECT2.getId();
        @NotNull final String taskId = USER2_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(userId, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(userId, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(userId, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(userId, projectId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(userId, "0", taskId));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                PROJECT_TASK_SERVICE.bindTaskToProject(userId, projectId, "0"));
        PROJECT_TASK_SERVICE.bindTaskToProject(userId, projectId, taskId);
        Assert.assertEquals(taskId, TASK_SERVICE.findAllByProjectId(userId, projectId).get(0).getId());
    }

    @Test
    public void unbindTaskFromProject() {
        @Nullable final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_PROJECT1.getId();
        @NotNull final String taskId = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, projectId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, "0", taskId));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, projectId, "0"));
        PROJECT_TASK_SERVICE.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertEquals(Collections.EMPTY_LIST, TASK_SERVICE.findAllByProjectId(userId, projectId));
    }

    @Test
    public void removeProjectById() {
        @Nullable final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(null, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById("", projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(userId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_TASK_SERVICE.removeProjectById(userId, ""));
        PROJECT_TASK_SERVICE.removeProjectById(userId, projectId);
        Assert.assertEquals(Collections.EMPTY_LIST, TASK_SERVICE.findAllByProjectId(userId, projectId));
    }

}
