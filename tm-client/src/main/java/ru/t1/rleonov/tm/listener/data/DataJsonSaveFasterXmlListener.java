package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataJsonFasterXmlRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataJsonSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-json-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Save data in json file using fasterxml.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerSaveDataJsonFasterXmlRequest request = new ServerSaveDataJsonFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
