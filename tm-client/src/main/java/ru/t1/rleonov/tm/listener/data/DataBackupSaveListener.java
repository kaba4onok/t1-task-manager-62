package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataBackupRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataBackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    public static final String DESCRIPTION = "Save backup in file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupSaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerSaveDataBackupRequest request = new ServerSaveDataBackupRequest(getToken());
        getDomainEndpoint().saveDataBackup(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
