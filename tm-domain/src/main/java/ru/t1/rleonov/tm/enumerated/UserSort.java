package ru.t1.rleonov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.comparator.NameComparator;
import ru.t1.rleonov.tm.comparator.StatusComparator;
import java.util.Comparator;

@Getter
public enum UserSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    UserSort(@NotNull String displayName,
             @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static UserSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final UserSort userSort : values()) {
            if (userSort.name().equals(value)) return userSort;
        }
        return null;
    }

}
