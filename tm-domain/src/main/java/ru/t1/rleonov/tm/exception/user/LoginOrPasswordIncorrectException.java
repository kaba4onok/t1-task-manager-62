package ru.t1.rleonov.tm.exception.user;

public final class LoginOrPasswordIncorrectException extends AbstractUserException {

    public LoginOrPasswordIncorrectException() {
        super("Error! Incorrect login or password. Please try again...");
    }

}
